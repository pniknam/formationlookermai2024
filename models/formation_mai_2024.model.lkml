connection: "formation_looker"
include: "/views/**/*.view.lkml"

explore: voiture {}

explore: vehiculesAH {
from: vehicules
  join: usagers {
      foreign_key: num_acc
      relationship: many_to_one # Could be excluded since many_to_one is the default
      type: left_outer          # Could be excluded since left_outer is the default
      }

  join: lieux {
    foreign_key: num_acc
    relationship: many_to_one # Could be excluded since many_to_one is the default
    type: left_outer          # Could be excluded since left_outer is the default
  }

  join: caracteristiques {
    foreign_key: num_acc
    relationship: many_to_one # Could be excluded since many_to_one is the default
    type: left_outer          # Could be excluded since left_outer is the default
  }

}


# explore: usagers {}
# explore: caracteristiques {}
# explore: lieux {}

explore: usagers {
  #from: usagers
  join: lieux {
    type:  left_outer
    relationship: many_to_one
    sql_on: ${usagers.num_acc}= ${lieux.num_acc}
    ;;}
  join: vehicules{
    type:  left_outer
    relationship: many_to_many
    sql_on: ${usagers.num_acc}= ${vehicules.num_acc} ;; }
  join: caracteristiques{
    type:  left_outer
    relationship: many_to_one
    sql_on: ${usagers.num_acc}= ${caracteristiques.num_acc} ;; }

}

explore: vehicules_vsb {
  from :  vehicules
  join: caracteristiques {
    relationship: many_to_one
    type: left_outer
    sql_on: ${vehicules_vsb.num_acc} = ${caracteristiques.num_acc} ;;
  }

  join: lieux {
    relationship: one_to_one
    type: left_outer
    sql_on: ${caracteristiques.num_acc} = ${lieux.num_acc} ;;
  }

  join: usagers {
    relationship: one_to_many
    type: left_outer
    sql_on: ${lieux.num_acc} = ${usagers.num_acc} ;;
  }
}


explore: vehicules {

  join: lieux {
    #sql: ${vehicules.num_acc} = ${lieux.num_acc} ;;
    foreign_key: num_acc
    relationship: many_to_one # Could be excluded since many_to_one is the default
    type: left_outer
  }
  join: usagers {
    #sql: ${vehicules.num_acc} = ${usagers.num_acc} ;;
    foreign_key: num_acc
    relationship: many_to_many # Could be excluded since many_to_one is the default
    type: left_outer
  }
  join: caracteristiques {
    #sql: ${vehicules.num_acc} = ${caracteristiques.num_acc} ;;
    foreign_key : num_acc
    relationship: many_to_one # Could be excluded since many_to_one is the default
    type: left_outer
  }
}
