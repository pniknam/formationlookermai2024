include: "/views/voiture.view.lkml" #Comme en Java
view: +voiture { #Permet d'hériter de la vue "Voiture"
  measure: count_distinct_model {
    type: count_distinct

    #sql: ${TABLE}.model n'est pas opti comme il s'agit d'une table fille, on peut directement appeler la valeur
    sql: ${model} ;;
    drill_fields: [model]
  }
  dimension: new_dealer_name {
    type : string
    sql:replace(${dealer_name}," ","-") ;;
  }

  dimension: new_fuel_type {
    case: {
      when: {
        label: "Gasoil"
        sql: ${fuel_type} = "DIESEL" ;;

      }
      when: {
        label: "Electrique"
        sql: ${fuel_type} = "ELECTRIC" ;;

      }
      when: {
        label: "Essence"
        sql: ${fuel_type} = "PETROL";;

      }
      when: {
        label: "GAZ"
        sql: ${fuel_type} = "PETROL CNGGAZ" OR ${fuel_type}="PETROL LPG";;
      }
      else: "unknown"
    }
  }
  set : complet {
    fields: [model,version,brand,catalogue_price]
  }
  dimension: concat_model_version {
    type: string
    sql: concat(${model},"_", ${version}) ;;
    drill_fields: [complet*]
  }

  dimension_group: new_format_invoice {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${invoice_date} ;;
    html:{{ rendered_value | date: "%A %d %h, %y" }};;
  }
}
