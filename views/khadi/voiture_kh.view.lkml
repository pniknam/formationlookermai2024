include: "/views/voiture.view.lkml"
view: +voiture {
  measure: count_distinct_model {
    type:  count_distinct
    #sql: ${TABLE}.model;;
    sql: ${model} ;;
    drill_fields: [complet*]
    }
  dimension: new_dealer_name {
    type: string
    sql: replace(${dealer_name}," ","-") ;;
  }
  dimension: fuel_type_fr{
    type:  string
    sql: case when ${fuel_type}= "DIESEL" then "Gasoil"
              when ${fuel_type}="ELECTRIC" then "Electrique"
              when ${fuel_type}="PETROL" then "ESSENCE"
              else "GAZ" end;;
  }

  dimension: concat_fields {
    sql: concat(${model},"TEST",${version}) ;;
    drill_fields: [complet*]
  }

  set: complet {
  fields: [model, brand, catalogue_price]
  }

  dimension_group: change_invoice{
    type: time
    sql: ${invoice_date} ;;
    html:{{ rendered_value | date: "%b %d, %y" }};;

  }
  }
