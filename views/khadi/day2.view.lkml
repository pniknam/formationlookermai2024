include: "/views/caracteristiques.view.lkml"
view: +caracteristiques {
  dimension: anciennete {
    type:  number
    sql: date_diff(current_date(), DATE (CONCAT(${an},"-",${mois},"-",${jour})), day)  ;;
  }
  }

#view:  +usagers {
 # dimension:   {}
#}
