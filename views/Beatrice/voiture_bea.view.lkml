include: "/views/voiture.view.lkml"
view: +voiture {
  measure: count_distinct_model {
    type: count_distinct
    sql: ${model};;
    drill_fields: [complet*]
  }
  dimension: dealer_name_replace {
    type: string
    sql: replace(${dealer_name}," ","-") ;;
  }
  dimension: fuel_type_french {
    type:  string
    sql: case ${fuel_type}
      when "DIESEL" then "Gasoil"
      when "ELECTRIC" then "Electrique"
      when "PETROL" then "Essence"
      else "GAZ"
      end;;
  }
  dimension: concat {
    type:  string
    sql:  concat(${model} , " " , ${version});;
    drill_fields: [complet*]
  }
  dimension: invoice_format {
    sql: ${invoice_date}  ;;
    html:{{ invoice_date | date: "%A %B %e %Y" }};;
  }
  dimension: catalogue_price_num {
    type:  number
    sql: ${catalogue_price} ;;
  }
  measure: catalogue_price_min {
    type: min
    value_format: "0.00€"
    sql: ${catalogue_price_num} ;;
  }
  measure: catalogue_price_max {
    type: max
    value_format: "0.00€"
    sql: ${catalogue_price_num} ;;
  }

  set: complet{
    fields: [brand, model, version, catalogue_price]
  }
  }
