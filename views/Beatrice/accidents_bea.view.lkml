include: "/views/accidents.view.lkml"
view: +accidents {

  dimension: anciennete {
    type: number
    sql: date_diff(current_date(), DATE( concat( ${caracteristiques.an}, "-" , ${caracteristiques.mois}, "-", ${caracteristiques.jour}), YEAR);;
  }

  dimension: age_usager {
    description: " "
    type: number
    sql: ${caracteristiques.an} - ${usagers.an_nais};;
  }


dimension: regroup_user_Age_accident{
    description: "ages des passager au moment d'accident"
    type: string
    sql:
      case
        when ${age_usager} <= 14 then 'Enfants '
        when ${age_usager} > 15 and  ${age_usager}  <= 24 then 'Adolescents '
        when ${age_usager} > 24 and  ${age_usager}   <= 64 then 'Adultes '
        when ${age_usager} > 65 then 'Aînés '
        else 'inconu'
      end;;

  }


  # # You can specify the table name if it's different from the view name:
  # sql_table_name: my_schema_name.tester ;;
  #
  # # Define your dimensions and measures here, like this:
  # dimension: user_id {
  #   description: "Unique ID for each user that has ordered"
  #   type: number
  #   sql: ${TABLE}.user_id ;;
  # }
  #
  # dimension: lifetime_orders {
  #   description: "The total number of orders for each user"
  #   type: number
  #   sql: ${TABLE}.lifetime_orders ;;
  # }
  #
  # dimension_group: most_recent_purchase {
  #   description: "The date when each user last ordered"
  #   type: time
  #   timeframes: [date, week, month, year]
  #   sql: ${TABLE}.most_recent_purchase_at ;;
  # }
  #
  # measure: total_lifetime_orders {
  #   description: "Use this for counting lifetime orders across many users"
  #   type: sum
  #   sql: ${lifetime_orders} ;;
  # }
}

# view: accidents_bea {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
