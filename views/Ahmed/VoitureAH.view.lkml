include: "/views/voiture.view.lkml"
view: +voiture {
  measure: count_distinct_model {
    type: count_distinct
#    sql: ${TABLE}.model ;;
    sql: ${model}  ;;
    drill_fields: [model]
  }

dimension: new_dealer_name{
 type: string
sql: REPLACE(${dealer_name}," ","_");;
}

dimension: fuel_type_caburant {
    type: string
    sql:case
    when ${fuel_type} = 'DIESEL' then 'Gasoil'
     when ${fuel_type} = 'ELECTRIQUE' then 'Electrique'
     when ${fuel_type} = 'PETROL' then 'Essence'
     when ${fuel_type} = 'PETROL CNGAZ' OR ${fuel_type} = 'PETROL GPL' then 'Gaz'
    else 'inconnue'
  end
    ;;

 }

  dimension: concat{
    type: string
    sql: CONCAT(${model}, 'tests', ${version})
    ;;
    drill_fields: [complet*]
  }
  set: complet {
    fields: [model,brand,version,catalogue_price]
  }


  dimension_group: invoice_date_format {

  #  timeframes: [time, date, week, month, raw]
    sql: ${invoice_date} ;;
    html:{{ rendered_value | date: "%b %d, %y" }};;

}
    dimension: field_name {
      type: location
      sql_latitude:36,8431;;
      sql_longitude:10,2783;;
    }


 }
