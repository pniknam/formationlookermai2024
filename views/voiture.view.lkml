view: voiture {
  sql_table_name: `sub-jms-gcp-for-looker.looker_formation.Voiture` ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }
  dimension: catalogue_price {
    type: number
    hidden:  yes
    sql: ${TABLE}.catalogue_price ;;
  }
  dimension: client_discount {
    type: string
    sql: ${TABLE}.client_discount ;;
  }
  dimension: dealer_name {
    type: string
    sql: ${TABLE}.dealer_name ;;
  }
  dimension: engine {
    type: string
    sql: ${TABLE}.engine ;;
  }
  dimension: fuel_type {
    type: string
    sql: ${TABLE}.fuel_type ;;
  }
  dimension_group: invoice {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.invoice_date ;;
  }
  dimension: marginal_profit {
    type: string
    sql: ${TABLE}.marginal_profit ;;
  }
  dimension: model {
    type: string
    sql: ${TABLE}.model ;;
  }
  dimension_group: order {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }
  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }
  dimension: version {
    type: string
    sql: ${TABLE}.version ;;
  }
  measure: count {
    type: count
    drill_fields: [dealer_name]
  }
}
