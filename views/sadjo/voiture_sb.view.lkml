include: "/views/voiture.view.lkml"
view: +voiture {
  measure: count_distinct_model {
    type: count_distinct
    sql: ${model} ;;
    drill_fields: [model]
  }

  dimension: new_dealer_name {
    type :  string
    sql: replace(${dealer_name}, " ", "_") ;;
  }

  dimension: type_de_carburant {
    type: string
    sql: CASE
          WHEN ${fuel_type} = "DIESEL" THEN "Gasoil"
          WHEN ${fuel_type} = "ELECTRIC" THEN "Electrique"
          WHEN ${fuel_type} = "PETROL" THEN "Essence"
          WHEN ${fuel_type} ="PETROL CNGGAZ" OR ${fuel_type} = "PETROL LPG" THEN "GAZ"
          ELSE "Inconnu"
          END
          ;;
  }

  dimension: group_name {
    type: string
    sql: concat(${model}, ${version}) ;;
    #drill_fields: [model, brand, version, catalogue_price]
    drill_fields: [complet*]
  }

  dimension: invoice_date_new_format  {
    sql: ${invoice_date} ;;
    html:{{ rendered_value | date: "%A %d %h, %y" }};;

  }
  # Pour ne pas repeter  le drill_fields à chaque fois
  set: complet {
    fields: [model, brand, version, catalogue_price]
  }
}
