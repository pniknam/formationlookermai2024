include: "/views/caracteristiques.view.lkml"
view: +caracteristiques {
  dimension: date_acc {
    description: "Date de l'accident (Réconstruite)"
    type: date
    sql: DATE(${caracteristiques.an}, ${caracteristiques.mois}, ${caracteristiques.jour}) ;;
  }
  dimension: anciennete_acc {
    description: "Ancienneté de l'accident (en nombre d'année)"
    type: number
    sql: DATE_DIFF(current_date(), ${caracteristiques.date_acc}, YEAR) ;;
  }
}
