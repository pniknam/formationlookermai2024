
view: runner_query_test1 {
  derived_table: {
    sql: SELECT
          voiture.dealer_name  AS voiture_dealer_name
      FROM `sub-jms-gcp-for-looker.looker_formation.Voiture`  AS voiture
      GROUP BY
          1
      ORDER BY
          1
      LIMIT 100 ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: voiture_dealer_name {
    type: string
    sql: ${TABLE}.voiture_dealer_name ;;
  }

  set: detail {
    fields: [
        voiture_dealer_name
    ]
  }
}
